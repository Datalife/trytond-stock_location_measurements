# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pyson import Eval, Bool, Id, Not, Equal
from trytond.pool import PoolMeta

__all__ = ['Location']

__metaclass__ = PoolMeta


class Location:
    __name__ = 'stock.location'

    length = fields.Float('Length', digits=(16, Eval('length_digits', 2)),
                          states={'invisible': Not(Equal(Eval('type'), 'storage')), },
                          depends=['type', 'length_digits'])
    length_uom = fields.Many2One('product.uom', 'Length Uom',
                                 domain=[('category', '=', Id('product', 'uom_cat_length'))],
                                 states={'invisible': Not(Equal(Eval('type'), 'storage')),
                                         'required': Bool(Eval('length')), },
                                 depends=['type', 'length'])
    length_digits = fields.Function(fields.Integer('Length Digits'),
                                    'on_change_with_length_digits')
    height = fields.Float('Height',
                          digits=(16, Eval('height_digits', 2)),
                          states={'invisible': Not(Equal(Eval('type'), 'storage')), },
                          depends=['type', 'height_digits'])
    height_uom = fields.Many2One('product.uom', 'Height Uom',
                                 domain=[('category', '=', Id('product', 'uom_cat_length'))],
                                 states={
                                     'invisible': Not(Equal(Eval('type'), 'storage')),
                                     'required': Bool(Eval('height')),
                                 },
                                 depends=['type', 'height'])
    height_digits = fields.Function(fields.Integer('Height Digits'),
                                    'on_change_with_height_digits')
    width = fields.Float('Width',
                         digits=(16, Eval('width_digits', 2)),
                         states={
                             'invisible': Not(Equal(Eval('type'), 'storage')),
                         },
                         depends=['type', 'width_digits'])
    width_uom = fields.Many2One('product.uom', 'Width Uom',
                                domain=[('category', '=', Id('product', 'uom_cat_length'))],
                                states={
                                    'invisible': Not(Equal(Eval('type'), 'storage')),
                                    'required': Bool(Eval('width')),
                                },
                                depends=['type', 'width'])
    width_digits = fields.Function(fields.Integer('Width Digits'),
                                   'on_change_with_width_digits')

    @fields.depends('length_uom')
    def on_change_with_length_digits(self, name=None):
        return (self.length_uom.digits if self.length_uom
                else self.default_length_digits())

    @staticmethod
    def default_length_digits():
        return 2

    @fields.depends('height_uom')
    def on_change_with_height_digits(self, name=None):
        return (self.height_uom.digits if self.height_uom
                else self.default_height_digits())

    @staticmethod
    def default_height_digits():
        return 2

    @fields.depends('width_uom')
    def on_change_with_width_digits(self, name=None):
        return (self.width_uom.digits if self.width_uom
                else self.default_width_digits())

    @staticmethod
    def default_width_digits():
        return 2

    @classmethod
    def view_attributes(cls):
        return super(Location, cls).view_attributes() + [
            ('//page[@id="measurements"]', 'states', {
                    'invisible': Not(Equal(Eval('type'), 'storage')),
                    })]